<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 7:53 PM
 */

namespace App\Http\Controllers\Admin;


use App\Demos\FizzBuzz\FizzBuzz;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class DemosController extends Controller
{

    public function getFizzBuzz()
    {
        $fizzbuzz = app()->make(FizzBuzz::class)->run();
        return view('admin.demos.fizzbuzz', compact('fizzbuzz'));
    }
    
    public function getTemplatePattern()
    {
        $vehicles = new Collection();
        foreach(['Airplane', 'Boat', 'Car', 'Hovercraft', 'Motorcycle'] as $baseClass) {
            $fullClassName = 'App\Demos\TemplatePattern\Vehicles\\'.$baseClass;
            $vehicles->push(new $fullClassName);
        }
        return view('admin.demos.template', compact('vehicles'));
    }
    
}