<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 6:46 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard', [
            'user' => \Auth::guard('admin')->user(),
        ]);
    }

}