<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 8:18 PM
 */

namespace App\Demos\TemplatePattern;


abstract class Vehicle
{

    # Abstract Methods, must be overridden

    /**
     * Return the top speed in Miles per Hour
     * @return mixed
     */
    abstract function getTopSpeedInMph();

    /**
     * Return the number of wheels on the vehicle
     * @return mixed
     */
    abstract function getWheelCount();

    /**
     * An array of surfaces the vehicle can travel on
     *   ex: land|air|water
     * @return array
     */
    abstract function getTravelSurfaces();


    # concrete methods, work for all subclasses, may not be overridden
    /**
     * Return the top speed in Kilometers Per Hour
     * @return mixed
     */
    public final function getTopSpeedInKph()
    {
        return $this->getTopSpeedInMph()*1.60934;
    }

    # base methods, children may override
    /**
     * Returns the name of the vehicle type
     *
     * @return string
     */
    public function getName()
    {
        return (new \ReflectionClass(static::class))->getShortName();
    }
}