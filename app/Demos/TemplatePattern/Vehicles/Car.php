<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 8:18 PM
 */

namespace App\Demos\TemplatePattern\Vehicles;


use App\Demos\TemplatePattern\Vehicle;

class Car extends Vehicle
{

    /**
     * Return the top speed in Miles per Hour
     * @return mixed
     */
    function getTopSpeedInMph()
    {
        return 180;
    }

    /**
     * Return the number of wheels on the vehicle
     * @return mixed
     */
    function getWheelCount()
    {
        return 4;
    }

    /**
     * An array of surfaces the vehicle can travel on
     *   ex: land|air|water
     * @return array
     */
    function getTravelSurfaces()
    {
        return ['land'];
    }
}