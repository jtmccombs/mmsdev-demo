<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 8:23 PM
 */

namespace App\Demos\TemplatePattern\Vehicles;


use App\Demos\TemplatePattern\Vehicle;

class Hovercraft extends Vehicle
{
    /**
     * Return the top speed in Miles per Hour
     * @return mixed
     */
    function getTopSpeedInMph()
    {
        return 125;
    }

    /**
     * Return the number of wheels on the vehicle
     * @return mixed
     */
    function getWheelCount()
    {
        return 0;
    }

    /**
     * An array of surfaces the vehicle can travel on
     *   ex: land|air|water
     * @return array
     */
    function getTravelSurfaces()
    {
        return ['land', 'water'];
    }
    
    public function getName()
    {
        return 'Magical Hovering Thingy';
    }
}