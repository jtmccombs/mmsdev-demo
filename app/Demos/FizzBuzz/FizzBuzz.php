<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 7:55 PM
 */

namespace App\Demos\FizzBuzz;


class FizzBuzz
{

    public function run()
    {
        $returnArray = [];
        for($i = 1; $i<=100; $i++) {

            if ($i % 5 == 0 && $i % 3 == 0) {
                $returnArray[$i] = 'FizzBuzz';
            }elseif ($i % 3 == 0) {
                $returnArray[$i] = 'Fizz';
            }elseif ($i % 5 == 0) {
                $returnArray[$i] = 'Buzz';
            }else {
                $returnArray[$i] = $i;
            }
        }
        return $returnArray;
    }

}