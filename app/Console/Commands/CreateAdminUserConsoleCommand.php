<?php

namespace App\Console\Commands;

use App\Auth\Admin\AdminUser;
use Illuminate\Console\Command;

class CreateAdminUserConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:create:admin {email} {password} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        
        if ( $this->userExists($email) ) {
            $this->error('Error: A user with the email address \''.$email.'\' already exists.');
            return;
        }

        $userData = [
            'email' => $email,
            'password' => $this->argument('password'),
        ];

        if ($name = $this->option('name') ) {
            $userData['name'] = $name;
        }

        $v = \Validator::make($userData, AdminUser::rules());

        if ($v->fails()) {
            $this->error('Error: Input fails validation:');
            foreach($v->errors()->getMessages() as $field => $errors) {
                $this->warn('   '.ucfirst($field).':');
                foreach($errors as $error) {
                    $this->warn('      '.$error);
                }
            }
            return;
        }
        
        $userData['password'] = \Hash::make($userData['password']);

        AdminUser::create($userData);

        $this->info('User ' . $email . ' created successfully.');
    }

    protected function userExists($email)
    {
        return (bool) AdminUser::where('email', '=', $email)->count();
    }
}
