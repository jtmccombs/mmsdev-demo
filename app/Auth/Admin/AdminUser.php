<?php
/**
 * Created by Justin McCombs.
 * Date: 4/12/16
 * Time: 6:04 PM
 */

namespace App\Auth\Admin;

use App\Auth\User;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends User
{

    protected $table = 'admin_users';
    
    protected static function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|min:8|max:32'
        ];
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
