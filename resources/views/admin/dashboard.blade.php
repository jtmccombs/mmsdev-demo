@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h2>Logged In Admin User:</h2>
            <pre>{!! $user !!}</pre>
        </div>
    </div>

    {{-- Code Sections --}}
    <div class="row">
        <div class="col-sm-4">
            <h3>FizBuzz</h3>
            <a href="{{ url('admin/demos/fizzbuzz') }}" class="btn btn-info">View</a>
        </div>

        <div class="col-sm-4">
            <h3>Template Pattern</h3>
            <a href="{{ url('admin/demos/template-pattern') }}" class="btn btn-info">View</a>
        </div>
    </div>
@stop