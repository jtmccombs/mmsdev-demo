@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h2>Vehicles</h2>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Top Speed</th>
                    <th>Travels On</th>
                    <th>Wheels</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($vehicles as $vehicle)
                    <tr>
                        <td>{{ $vehicle->getName() }}</td>
                        <td>{{ $vehicle->getTopSpeedInMph() }} mph / {{ $vehicle->getTopSpeedInKph() }} kph</td>
                        <td>{{ implode(', ', $vehicle->getTravelSurfaces()) }}</td>
                        <td>{{ $vehicle->getWheelCount() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Code Sections --}}
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ url('admin') }}" class="btn btn-info">Back to dashboard</a>
        </div>
    </div>
@stop