@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h2>Fizzbuzz Results</h2>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Result</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($fizzbuzz as $key => $fizz)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $fizz }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Code Sections --}}
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ url('admin') }}" class="btn btn-info">Back to dashboard</a>
        </div>
    </div>
@stop