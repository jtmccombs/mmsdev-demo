<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MerchantMatch Demo</title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
    <body>
        <div class="container">

            @yield('content')

            <hr>

            <div class="row well">
                <div class="col-sm-12">
                    <a class="btn btn-warning pull-right" href="{{ url('admin/logout') }}">Log Out</a>
                </div>
            </div>
        </div>
    </body>
</html>